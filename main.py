import json
import datetime
import sentry_sdk

from fastapi import FastAPI
from mangum import Mangum
from sentry_sdk.integrations.serverless import serverless_function

from typing import List, Optional
from pydantic import BaseModel, EmailStr

app = FastAPI()

sentry_sdk.init(dsn="https://7190416f85824370a90dfc74b3d22a8d@o508276.ingest.sentry.io/5600545")


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    tags: List[str] = []


class Comercial(BaseModel):
    id: int
    usuario: int
    nome: str


class Regional(BaseModel):
    id: int
    usuario: int
    nome: str


class Filial(BaseModel):
    id: int
    usuario: int
    nome: str


class Sala(BaseModel):
    comercial: Comercial
    regional: Regional
    filial: Filial
    estado: str


class Corretor(BaseModel):
    id: int
    usuario: int
    nome: str
    nome_fantasia: str
    telefone_comercial: str
    telefone_residencial: str
    celular: str
    sala: Sala


class Identidade(BaseModel):
    numero: int
    orgao_emissor: str
    uf: str
    data_emissao: str


class Endereco(BaseModel):
    bairro: str
    cidade: str
    uf: str
    cep: str
    logradouro: str
    numero: int
    complemento: str


class Naturalidade(BaseModel):
    cidade: str
    uf: str


class Pagamento(BaseModel):
    conta_tipo: str
    conta_tipo_nome: str
    tipo: str
    tipo_nome: str
    conta_digito: int
    conta: int
    agencia_digito: int
    agencia: int
    banco: int
    banco_codigo_febraban: int
    banco_nome: str


class Orgao(BaseModel):
    codigo: int
    nome: str
    sub_orgao: int
    matricula: int
    senha: int
    beneficio_especie: int
    beneficio_uf: str
    recebe_beneficio_cartao: bool
    possui_beneficio: bool


class Telefone(BaseModel):
    tipo: str
    numero: str


class Cliente(BaseModel):
    cpf: str
    nome: str
    data_nascimento: str
    pai_nome: str
    mae_nome: str
    email: str
    salario: float
    senha_contra_cheque: str
    sexo: str
    identidade: Identidade
    endereco: Endereco
    naturalidade: Naturalidade
    pagamento: Pagamento
    orgao: Orgao
    telefones: List[Telefone]


class Arquivo(BaseModel):
    nome: str
    url: str
    tipo: str
    data: str
    ed_up_id: int
    cod_ff: str


class Contrato(BaseModel):
    codigo: str
    ade: int
    banco: int
    banco_nome: str
    banco_codigo_febraban: int
    operacao_codigo: int
    operacao_nome: str
    tabela_codigo: int
    tabela_nome: str
    tabela_produto: float
    margem: float
    prazo: int
    parcela: float
    coeficiente: float
    valor_operacao: float
    valor_liberado: float
    producao_bruta: float
    producao_liquida: float
    observacao: str
    data_cadastro: str
    data_pagamento: str
    consulta_online: int
    formalizacao_digital: bool
    corretor: Corretor
    atendente: List
    proposta_bmg: bool
    digitador: str
    produtos_agregados: List
    cliente: Cliente
    arquivos: List[Arquivo]


@app.get("/")
def hello():
    current_time = datetime.datetime.now().time()
    body = {
        "message": "Hello, the current time is " + str(current_time)
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    return response


@app.post("/contract/", response_model=Contrato)
async def create_contract(item: Item):
    return item


@app.post("/items/", response_model=Item)
async def create_item(item: Item):
    return item


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


@serverless_function
@app.get("/verify/{value}")
def verify(value: int):
    division_by_zero = value / 0
    return division_by_zero


handler = Mangum(app)
