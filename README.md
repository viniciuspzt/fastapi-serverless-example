# FastAPI-Serverless-Example
FastAPI, Mangum, Serverless, Sentry

### Virtual env:
Create virtualenv - python -m venv ./venv   <br/>
Activate venv - source venv/bin/activate    <br/>
Deactivate - deactivate                     <br/>

## Installation:
pip install fastapi                         <br/>
pip install uvicorn[standard]               <br/>
pip install mangum                          <br/>
npm install -g serverless

## Run local:
uvicorn main:app --reload

## Deploy:
serverless deploy

### Links:
https://fastapi.tiangolo.com/

https://mangum.io/

https://www.serverless.com/

https://github.com/jordaneremieff/serverless-mangum-examples/tree/main/fastapi-example

https://github.com/serverless/examples/tree/master/aws-python-simple-http-endpoint

https://docs.sentry.io/platforms/python/guides/asgi/

https://docs.sentry.io/platforms/python/guides/serverless/